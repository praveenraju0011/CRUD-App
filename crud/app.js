const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser')
const homeRoutes = require('./routers/home');

const app = express();

const port = process.env.port || 5000;




//db connection
mongoose.connect("mongodb://0.0.0.0:27017/studentdetails", {useNewUrlParser : true})
        

const db = mongoose.connection;
db.on('error',()=>{
    console.log("Err is ");
})

db.once('open', ()=>{
    console.log("connected");
})



//view engine

app.set('view engine','ejs');
app.use(express.static('public'))


//body parser
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

//home page

app.use('/' , homeRoutes);



// app.get('/', (req , res)=>{
//     res.send('Hello 5000 port');
// })






//Port
app.listen(port);
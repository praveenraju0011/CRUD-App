const express = require('express');
const Club = require('../models/club');
const Router = express.Router();


Router.get('/', (err, res) => {
    res.render('index');

})


//Create

Router.post('/add', (req, res) => {
    const name = req.body.name;
    const email = req.body.email;
    const phone = req.body.number;
    const password = req.body.password;
  
    //validation
    Club.find({ email }, (err, docs) => {
        if (err) {
            console.log("err")
        }
        else {

            if (!docs[0]) {

                //creation of new element in DB
                const club = new Club({
                    name,
                    email,
                    phone,
                    password
                })

                club.save(err => {
                    if (err) {
                        console.log(err);
                    }
                    else {
                        // console.log("success")

                        Club.find({ email }, (err, docs) => {
                            if (err) {
                                console.log("err")
                            }
                            else {
                                //console.log("docs", docs);
                                res.render('serachbar', {
                                    studentemail: docs

                                })
                            }
                        })
                    }
                })
            }

            //error if validation fails
            else {
                res.render('error', {
                    error: docs
                })

            }



        }
    })

})



//Read

Router.get('/show', (req, res) => {
    Club.find((err, docs) => {
        if (err) {
            throw err;
        }

        //console.log(docs);
        res.render('show', {
            students: docs
        })


    })
})



//update


Router.get('/edit/:id', (req, res) => {
    //console.log(req.params.id);

    Club.findOneAndUpdate({ phone: req.params.id }, req.body, { new: true }, (err, docs) => {
        if (err) {
            console.log("cant update")
        }
        else {
            // console.log("done");

            res.render('edit', {
                studentdata: docs

            })
        }
    })
})

Router.post('/edit/:id', (req, res) => {
    //console.log(req.params.id);

    Club.findByIdAndUpdate({ _id: req.params.id }, req.body, (err, docs) => {
        if (err) {
            console.log("err")
        }
        else {
            //console.log("done");
            res.redirect('/show')
        }
    })
})


//delete
Router.get('/delete/:id', (req, res) => {
    //console.log(req.params.id);

    Club.findByIdAndDelete({ _id: req.params.id }, req.body, (err, docs) => {
        if (err) {
            console.log("err")
        }
        else {
            // console.log("done");
            res.redirect('/show')
        }
    })
})



//searchbar
Router.post('/email', (req, res) => {
    const email = req.body.email;
    req.body.name = ''
    //console.log(req.body);

    Club.find({ email }, (err, docs) => {
        if (err) {
            console.log("err")
        }
        else {
            //console.log("docs", docs);
            res.render('serachbar', {
                studentemail: docs

            })
        }
    })

})







module.exports = Router;